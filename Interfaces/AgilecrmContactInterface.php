<?php

namespace Pl\AgilecrmBundle\Interfaces;

interface AgilecrmContactInterface
{
	/**
	 * @return string
	 */
	public function getAgilecrmContactId();

	/**
	 * @param string $id
	 * @return mixed
	 */
	public function setAgilecrmContactId($id);


	/**
	 * @return string
	 */
	public function getPrenom();

	/**
	 * @return string
	 */
	public function getNom();

	/**
	 * @return string
	 */
	public function getEmail();
}