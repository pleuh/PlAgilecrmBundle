<?php

namespace Pl\AgilecrmBundle\Interfaces;

interface AgilecrmOpportunityInterface
{
	/**
	 * @return string
	 */
	public function getAgilecrmOpportunityId();
	/**
	 * @return string
	 */
	public function getAgilecrmOpportunityName();

	/**
	 * @param string $id
	 * @return mixed
	 */
	public function setAgilecrmOpportunityId($id);

	/**
	 * @return string
	 */
	public function getAgilecrmOpportunityMilestone();

	/**
	 * @return string
	 */
	public function getAgilecrmOpportunityExpectedValue();

}