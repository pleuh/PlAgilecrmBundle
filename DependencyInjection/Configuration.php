<?php

namespace Pl\AgilecrmBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder
            ->root('pl_agilecrm')
            ->children()
            ->scalarNode('api_key')->end()
            ->scalarNode('domain')->end()
            ->scalarNode('email')->end()
            ->end()
            ;
        return $treeBuilder;
    }
}
