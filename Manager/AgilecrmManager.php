<?php


namespace Pl\AgilecrmBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Pl\AgilecrmBundle\Exception\AgileCrmException;
use Pl\AgilecrmBundle\Interfaces\AgilecrmContactInterface;
use Pl\AgilecrmBundle\Interfaces\AgilecrmOpportunityInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * Class StripeManager
 * @package Pl\AgilecrmBundle\Manager
 * @property string $apiKey
 * @property string $domain
 * @property string $email
 * @property EntityManagerInterface $em
 */
class AgilecrmManager
{
	protected $apiKey;
	protected $domain;
	protected $email;
	protected $em;

	public function __construct($apiKey, $domain, $email, EntityManagerInterface $em){
		$this->apiKey = $apiKey;
		$this->domain = $domain;
		$this->email = $email;
		$this->em = $em;
	}

	/**
	 * @param AgilecrmContactInterface $agilecrmContactInterface
	 * @param bool $bPersistAndFlush
	 * @return string|void
	 * @throws AgileCrmException
	 */
	public function getAndStoreContactId(AgilecrmContactInterface $agilecrmContactInterface, $bPersistAndFlush = true){
		if(!$agilecrmContactInterface->getAgilecrmContactId()){
			if(preg_match('#[ ;]#', $agilecrmContactInterface->getEmail())){
				return null;
			}
			$response = $this->sendCall("contacts", [
				"properties" => [
					[
						"type" => "SYSTEM",
						"name" => "first_name",
						"value" => $agilecrmContactInterface->getPrenom(),
					],
					[
						"type" => "SYSTEM",
						"name" => "last_name",
						"value" => $agilecrmContactInterface->getNom(),
					],
					[
						"type" => "SYSTEM",
						"name" => "email",
						"subtype" => "work",
						"value" => $agilecrmContactInterface->getEmail(),
					],
				]
			], Request::METHOD_POST);

			if(preg_match("#duplicate#", $response) || preg_match("#already exists#", $response)){
				//Search contact by id
				$response = json_decode($this->sendCall("contacts/search/email/" . preg_replace('# #', "+", $agilecrmContactInterface->getEmail())));
			}
			if(!$response || !is_object($response) || !property_exists($response, "id")){
				if(is_array($response)){
					$response = serialize($response);
				}
				throw new AgileCrmException("Impossible de récupérer l'id pour le contact : " . $agilecrmContactInterface->getEmail() . " : " . ($response ?? "reponse null"));
			}


			$agilecrmContactInterface->setAgilecrmContactId($response->id);
			if($bPersistAndFlush){
				$this->em->persist($agilecrmContactInterface);
				$this->em->flush();
			}
		}
		return $agilecrmContactInterface->getAgilecrmContactId();
	}


	/**
	 * @param AgilecrmOpportunityInterface $agilecrmOpportunityInterface
	 * @param bool $bPersistAndFlush
	 * @return string|void
	 * @throws AgileCrmException
	 */
	public function createAndSetOpportunityId(AgilecrmOpportunityInterface $agilecrmOpportunityInterface, $bPersistAndFlush = true){
		if(!$agilecrmOpportunityInterface->getAgilecrmOpportunityId()){
			$response = json_decode($this->sendCall("opportunity", [
				"name" => $agilecrmOpportunityInterface->getAgilecrmOpportunityName(),
				"expected_value" => $agilecrmOpportunityInterface->getAgilecrmOpportunityExpectedValue(),
				"milestone" => $agilecrmOpportunityInterface->getAgilecrmOpportunityMilestone(),
			], Request::METHOD_POST));



			$agilecrmOpportunityInterface->setAgilecrmOpportunityId($response->id);
			if($bPersistAndFlush){
				$this->em->persist($agilecrmOpportunityInterface);
				$this->em->flush();
			}
		}
	}




	public function sendCall($path, $data = [], $method = Request::METHOD_GET, $content_type = "application/json") {
		$data = json_encode($data);

		$agile_url = "https://" . $this->domain . ".agilecrm.com/dev/api/" . $path;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($ch, CURLOPT_UNRESTRICTED_AUTH, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_URL, $agile_url);

		switch ($method) {
			case Request::METHOD_GET:
			case Request::METHOD_DELETE:
				break;
			case Request::METHOD_POST:
			case Request::METHOD_PUT:
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				break;
			default:
				break;
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			"Content-type : $content_type;", 'Accept : application/json'
		]);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, $this->email . ':' . $this->apiKey);
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}


}
